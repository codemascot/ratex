# Ratex
A small REST API built with Python Flask framework.

Time: Around 5 hours.

## Installation
1. After clonning the repo, copy the `.env.example` as `.env`
```
cp .env.example .env
```
2. Run the project with Docker.
```
docker-compose up --build
```

## Running
Visit this below URL from browser or you can also use `HTTPie` or `cURL` to access it
```
http://127.0.0.1:5157/rates?date_from=2016-01-01&date_to=2016-01-10&origin=CNSGH&destination=north_europe_main
```
**NB: The port is `5157`! So the base URL is `http://127.0.0.1:5157`!** 


## Test
To run the tests use below command:
```
docker-compose exec web pytest -v
```
**NB:** There are **40 tests here**, of which **36 are unit tests** and **4 are integration tests**. All the unit tests get run in **isolation**, means **mocking** feature of unit testing library has been leveraged. In short dependencies has been **mocked** while testing the functions! Rest **4 integration tests** depends on the DB and test client which are all for API testing with response.
