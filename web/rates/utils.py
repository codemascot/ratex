from datetime import datetime


def valid_date(date_str: str) -> bool:
    """
    Returns whether a date is valid or not.

    Args:
        date_str (str): The date string to check in the format "YYYY-MM-DD".

    Returns:
        bool: True if the date string is valid, False otherwise.
    """
    try:
        datetime.strptime(date_str, "%Y-%m-%d")
        return True
    except ValueError:
        return False


def valid_date_range(date1: str, date2: str) -> bool:
    """
    Returns whether a date range is valid or not.

    Args:
        date1 (str): The starting date string of the range in the format "YYYY-MM-DD".
        date2 (str): The ending date string of the range in the format "YYYY-MM-DD".

    Returns:
        bool: True if the date range is valid, False otherwise.
    """
    dt1 = datetime.strptime(date1, "%Y-%m-%d")
    dt2 = datetime.strptime(date2, "%Y-%m-%d")

    return dt1 <= dt2


def has_special_char(s: str) -> bool:
    """
    Returns whether a string contains special characters or not.

    Args:
        s (str): The string to check.

    Returns:
        bool: True if the string contains special characters, False otherwise.
    """
    for c in s:
        if not (c.isalpha() or c.isdigit() or c == "_"):
            return True
    return False


def validate_params(params: dict) -> str | None:
    """
    Validates parameters and returns an error message if invalid.

    Args:
        params (dict): A dictionary containing the parameters to validate.

    Returns:
        str or None: An error message if any of the parameters are invalid, None otherwise.
    """
    # Date keys for the elements in params
    dk = ["date_to", "date_from"]

    for k, v in params.items():
        if (
            not v
            or (k in dk and not valid_date(v))
            or (k not in dk and has_special_char(v))
        ):
            return f"Invalid `{k}` argument"

    if not valid_date_range(params["date_from"], params["date_to"]):
        return "Invalid date range"

    return None


def no_data(nested_arr: list[list[any]]) -> bool:
    """
    Returns whether there is no data in a nested array or not.

    Args:
        nested_arr (list[list[any]]): A nested list containing data.

    Returns:
        bool: True if there is no data, False otherwise.
    """
    for arr in nested_arr:
        if arr[1] is not None:
            return False
    return True
