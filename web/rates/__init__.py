import os

from typing import Optional
from flask import Flask, g
from rates.api import rates_bp
from rates.db import create_conn_pool
from rates.config import config

def create_app(app_config: Optional[dict[str, any]] = None) -> Flask:

    app = Flask(__name__, instance_relative_config=True)

    app.config.from_prefixed_env()
    
    app.config.from_mapping(
        SECRET_KEY = app.config["SECRET"] # To keep the data safe
    )

    # set configuration values
    if app_config is None:
        app_config = config[app.config["ENVIRONMENT"]]
    # loading config
    app.config.from_object(app_config)

    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    @app.before_request
    def init_conn_pool():
        # DB connection pool init
        g.conn_pool = getattr(g, 'conn_pool', None) or create_conn_pool()


    @app.teardown_appcontext
    def close_conn_pool(exception):
        conn_pool = g.pop('conn_pool', None)
        if conn_pool is not None:
            conn_pool.closeall()
            
    # registering the rates api blueprint
    app.register_blueprint(rates_bp)

    return app
