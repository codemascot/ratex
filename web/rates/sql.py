# COALESCE has been used here to simplify the logic in the Python end.
PORTS_QUERY = """
WITH RECURSIVE
zone AS (
    SELECT slug
    FROM regions
    WHERE slug = %(slug)s

    UNION ALL

    SELECT rgn.slug
    FROM regions AS rgn
    INNER JOIN zone ON zone.slug = rgn.parent_slug
)
SELECT COALESCE(ARRAY_AGG(ports.code), ARRAY[%(slug)s])
FROM ports
INNER JOIN zone ON zone.slug = ports.parent_slug;
"""

AVG_PRICE_QUERY = """
WITH
days AS (
    SELECT day::date FROM generate_series(
        %(date_from)s::timestamp,
        %(date_to)s::timestamp,
        '1 day'::interval
    ) AS day
),
rates AS (
    SELECT AVG(price) AS price, day, COUNT(*) AS freq
    FROM prices
    WHERE orig_code IN %(orig_code)s AND
          dest_code IN %(dest_code)s AND
          day BETWEEN %(date_from)s AND %(date_to)s
    GROUP BY day
)
SELECT TO_CHAR(days.day, 'YYYY-MM-DD'),
       CASE WHEN rates.freq < 3 THEN null ELSE ROUND(rates.price) END
FROM days
LEFT JOIN rates ON rates.day = days.day;
"""
