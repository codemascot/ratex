import logging

from werkzeug.exceptions import InternalServerError
from psycopg2 import pool
from flask import current_app, g


def create_conn_pool() -> pool.SimpleConnectionPool:
    """
    Create a connection pool to the database using the configuration settings from the Flask app.

    Returns:
        pool.SimpleConnectionPool: A connection pool object.
    """
    conn_pool = None
    try:
        conn_pool = pool.SimpleConnectionPool(
            minconn=current_app.config["MIN_DB_CONN"],
            maxconn=current_app.config["MAX_DB_CONN"],
            user=current_app.config["USER"],
            password=current_app.config["PASSWORD"],
            host=current_app.config["HOST"],
            port=current_app.config["DB_PORT"],
            database=current_app.config["DB"])
        if conn_pool:
            logging.info("Connection pool created successfully")
    except Exception as e:
        logging.error(f"Error in creating connection pool: {e}")
    return conn_pool



def select(query: str, params: dict) -> list:
    """
    Executes a SELECT query on the database and returns the result.

    Args:
        query: A string containing the SELECT query to execute.
        params: A dictionary containing any parameters that should be substituted in the query.

    Returns:
        A list of tuples containing the result of the SELECT query.

    Raises:
        InternalServerError: If an error occurs while executing the query.
    """
    try:
        with g.conn_pool.getconn() as conn, conn.cursor() as cursor:
            cursor.execute(query, params)
            result = cursor.fetchall()
    except Exception as e:
        logging.error(f"Error in executing query: {e}")
        raise InternalServerError("An internal server error occurred!")

    return result
