from rates.sql import PORTS_QUERY, AVG_PRICE_QUERY
from rates.db import select


def get_avg_prices(params: dict) -> list[tuple[str, int]]:
    """
    Retrieves the average prices for a given origin and destination port
    within a specified date range.

    Args:
        params (dict): A dictionary containing the following keys:
            - origin (str): The slug of the origin port
            - destination (str): The slug of the destination port
            - date_from (str): The start date of the date range in format 'YYYY-MM-DD'
            - date_to (str): The end date of the date range in format 'YYYY-MM-DD'

    Returns:
        List of tuples containing the average prices of each day within the specified date range
        for the given origin and destination port. Each tuple contains two elements: a string
        representing the date in format 'YYYY-MM-DD', and an integer representing the average
        price on that day in USD.
    """
    # Retrieve the code for the origin port
    orig = select(
        PORTS_QUERY,
        {'slug': params['origin']}
    )
    
    # Retrieve the code for the destination port
    dest = select(
        PORTS_QUERY,
        {'slug': params['destination']}
    )

    # Retrieve the average prices for the given origin, destination, and date range
    return select(AVG_PRICE_QUERY, {
        'date_from': params['date_from'],
        'date_to': params['date_to'],
        'orig_code': tuple(orig[0][0]),
        'dest_code': tuple(dest[0][0])
    })



"""
With a helper function performance can be enhanced based on the
assumption that none of the slug is less than 6 characters or
upper case as the below query returned empty:

SELECT slug, parent_slug
FROM regions
WHERE (LENGTH(slug) < 6 OR (parent_slug IS NOT NULL AND LENGTH(parent_slug) < 6))
AND (slug = UPPER(slug) OR parent_slug = UPPER(parent_slug));

But for now skipping this as we need more info on the schema to be sure on this assumption!
"""
