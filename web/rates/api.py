from flask import Blueprint, request
from werkzeug.exceptions import NotFound, BadRequest
from rates.service import get_avg_prices
from rates.utils import validate_params, no_data

rates_bp: Blueprint = Blueprint('rates', __name__, url_prefix='/rates')

@rates_bp.route('/', methods=['GET'])
def rates() -> list[dict[str, any]]:
    params = {
        'origin': request.args.get('origin'),
        'destination': request.args.get('destination'),
        'date_from': request.args.get('date_from'),
        'date_to': request.args.get('date_to')
    }

    error = validate_params(params)
    if error is not None:
        raise BadRequest(error)

    # Getting the average prices
    res = get_avg_prices(params)

    if no_data(res):
        raise NotFound("No data found!"); # Throwing a 404, but could be 204 as well

    return [{'day': day, 'average_price': avg_price} for day, avg_price in res]
