import pytest
from rates import create_app
from rates.config import config


@pytest.fixture()
def app():
    app = create_app(config['testing'])
    yield app


@pytest.fixture()
def client(app):
    return app.test_client()
