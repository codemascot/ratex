import pytest

from rates.utils import valid_date, valid_date_range, has_special_char, validate_params, no_data


@pytest.mark.parametrize(
    "date_str, expected",
    [
        ("2022-01-01", True),
        ("2022-02-29", False),
        ("2022-04-31", False),
        ("2022-13-01", False),
        ("2022-01-32", False),
        ("2022/01/01", False),
    ],
)
def test_valid_date(date_str, expected):
    assert valid_date(date_str) == expected


@pytest.mark.parametrize(
    "date1, date2, expected",
    [
        ("2022-01-01", "2022-01-02", True),
        ("2022-01-02", "2022-01-01", False),
        ("2022-01-01", "2021-01-01", False),
        ("2022-01-01", "2022-01-01", True),
    ],
)
def test_valid_date_range(date1, date2, expected):
    assert valid_date_range(date1, date2) == expected


@pytest.mark.parametrize(
    "s, expected",
    [
        ("abcd1234", False),
        ("ab_cd1234", False),
        ("ab-cd1234", True),
        ("ab%cd1234", True),
        ("ab cd1234", True),
        ("", False),
    ],
)
def test_has_special_char(s, expected):
    assert has_special_char(s) == expected


@pytest.mark.parametrize(
    "params, expected",
    [
        (
            {"date_from": "2022-01-01", "date_to": "2022-01-02", "origin": "some_origin", "destination": "some_destination"},
            None
        ),
        (
            {"date_from": "2022-01-01", "date_to": "2022-01-02", "origin": "some_origin", "destination": "some_destination; DROP DATABASE;"},
            "Invalid `destination` argument",
        ),
        (
            {"date_from": "2022-01-01", "date_to": "2022-01-02", "origin": "some_origin; DROP DATABASE;", "destination": "some_destination"},
            "Invalid `origin` argument",
        ),
        (
            {"date_from": "2022-01-01", "date_to": "2022-01-02", "origin": "", "destination": "some_destination"},
            "Invalid `origin` argument",
        ),
        (
            {"date_from": "2022-01-32", "date_to": "2022-02-01", "origin": "some_origin", "destination": "some_destination"},
            "Invalid `date_from` argument",
        ),
        (
            {"date_from": "2022-02-01", "date_to": "2022-01-01", "origin": "some_origin", "destination": "some_destination"},
            "Invalid date range",
        ),
        (
            {"date_from": "2022-01-01", "date_to": "2022-01-02", "origin": "some_origin", "destination": "some_destination"},
            None,
        ),
        (
            {"date_from": "2022-01-01", "date_to": "2022-01-02", "origin": "SOMEORIGIN", "destination": "some_destination"},
            None,
        ),
        (
            {"date_from": "2022-01-01", "date_to": "2022-01-02", "origin": "SOMEORIGIN", "destination": "ADESTINATION"},
            None,
        ),
        (
            {"date_from": "2022-01-01", "date_to": "2022-01-02", "origin": "some_origin", "destination": "ADESTINATION"},
            None,
        ),
    ],
)
def test_validate_params(params, expected):
    assert validate_params(params) == expected


@pytest.mark.parametrize(
    "nested_arr, expected",
    [
        ([[1, None], [2, None]], True),
        ([[1, None], [2, "foo"]], False),
        ([[1, "bar"], [2, "foo"]], False),
        ([], True),
    ],
)
def test_no_data(nested_arr, expected):
    assert no_data(nested_arr) == expected
