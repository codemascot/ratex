import pytest

from werkzeug.exceptions import InternalServerError
from unittest.mock import MagicMock, patch
from rates.db import select, create_conn_pool


@pytest.fixture(scope="function")
def mock_app_config():
    # Set up mock config values
    return {
        "MIN_DB_CONN": 2,
        "MAX_DB_CONN": 10,
        "USER": "test_user",
        "PASSWORD": "test_password",
        "HOST": "test_host",
        "DB_PORT": 5432,
        "DB": "test_db"
    }


def test_create_conn_pool_success(mock_app_config):
    # Mock the current_app.config object to return the mock config values
    mock_app = MagicMock()
    mock_app.config = mock_app_config
    mock_pool = MagicMock()
    mock_SimpleConnectionPool = MagicMock()
    mock_logging = MagicMock()
    mock_logging_info = MagicMock()
    mock_logging_error = MagicMock()

    mock_pool.SimpleConnectionPool.return_value = mock_SimpleConnectionPool
    mock_logging.info.return_value = mock_logging_info
    mock_logging.error.return_value = mock_logging_error

    with patch('rates.db.pool', mock_pool), patch('rates.db.current_app', mock_app), patch('rates.db.logging', mock_logging):
        result = create_conn_pool()
        assert result == mock_SimpleConnectionPool
        mock_pool.SimpleConnectionPool.assert_called_once()
        mock_logging.info.assert_called_once()
        mock_logging.error.assert_not_called()


def test_create_conn_pool_error(mock_app_config):
    # Mock the current_app.config object to return the mock config values
    mock_app = MagicMock()
    mock_app.config = mock_app_config
    mock_pool = MagicMock()
    mock_SimpleConnectionPool = Exception("Something went wrong")
    mock_logging = MagicMock()
    mock_logging_info = MagicMock()
    mock_logging_error = MagicMock()

    mock_pool.SimpleConnectionPool.side_effect = mock_SimpleConnectionPool
    mock_logging.info.return_value = mock_logging_info
    mock_logging.error.return_value = mock_logging_error

    with patch('rates.db.pool', mock_pool), patch('rates.db.current_app', mock_app), patch('rates.db.logging', mock_logging):
        result = create_conn_pool()
        assert result == None
        mock_pool.SimpleConnectionPool.assert_called_once()
        mock_logging.info.assert_not_called()
        mock_logging.error.assert_called_once()


def test_select_success():
    mock_g = MagicMock()
    mock_conn_pool = MagicMock()
    mock_conn = MagicMock()
    mock_cursor = MagicMock()
    mock_execute = MagicMock()

    mock_g.conn_pool = mock_conn_pool
    mock_conn_pool.getconn.return_value.__enter__.return_value = mock_conn
    mock_conn.cursor.return_value.__enter__.return_value = mock_cursor
    mock_cursor.execute.return_value = mock_execute
    mock_cursor.fetchall.return_value = [[1, 'test'], [2, 'data']]

    with patch('rates.db.g', mock_g):
        result = select("SELECT * FROM test_table WHERE id = %(id)s", {'id': 1})
        assert result == mock_cursor.fetchall.return_value
        mock_g.conn_pool.getconn.assert_called_once()
        mock_conn.cursor.assert_called_once()
        mock_cursor.execute.assert_called_once_with("SELECT * FROM test_table WHERE id = %(id)s", {'id': 1})
        mock_cursor.fetchall.assert_called_once()


def test_select_error():
    mock_g = MagicMock()
    mock_conn_pool = MagicMock()

    mock_g.conn_pool = mock_conn_pool
    mock_conn_pool.getconn.return_value.__enter__.side_effect = Exception("Something went wrong")

    with patch('rates.db.g', mock_g):
        try:
            select("SELECT * FROM test_table WHERE id = %(id)s", {'id': 1})
        except InternalServerError as e:
            assert str(e) == "500 Internal Server Error: An internal server error occurred!"
        else:
            assert False, "InternalServerError not raised"
