from unittest.mock import call, patch, MagicMock
from rates.service import get_avg_prices
from rates.sql import PORTS_QUERY, AVG_PRICE_QUERY


def test_get_avg_prices():
    mock_select = MagicMock()
    mock_select.side_effect = [
        [[['CNSGH']]],
        [[['NLRTM','BEZEE','FRLEH','DEBRV','BEANR','GBFXT','GBSOU','DEHAM']]],
        [
            ['2016-01-01', '1112'],
            ['2016-01-02', '1112'],
            ['2016-01-03', None],
            ['2016-01-04', None],
            ['2016-01-05', '1142'],
            ['2016-01-06', '1142'],
            ['2016-01-07', '1137'],
            ['2016-01-08', '1124'],
            ['2016-01-09', '1124'],
            ['2016-01-10', '1124']
        ]
    ]
    with patch('rates.service.select', mock_select):
        params = {
            'origin': 'CNSGH',
            'destination': 'north_europe_main',
            'date_from': '2022-01-01',
            'date_to': '2022-01-10'
        }
        result = get_avg_prices(params)
        assert result == [
            ['2016-01-01', '1112'],
            ['2016-01-02', '1112'],
            ['2016-01-03', None],
            ['2016-01-04', None],
            ['2016-01-05', '1142'],
            ['2016-01-06', '1142'],
            ['2016-01-07', '1137'],
            ['2016-01-08', '1124'],
            ['2016-01-09', '1124'],
            ['2016-01-10', '1124']
        ]
        mock_select.assert_has_calls([
            call(PORTS_QUERY, {'slug': 'CNSGH'}),
            call(PORTS_QUERY, {'slug': 'north_europe_main'}),
            call(AVG_PRICE_QUERY, {
                'date_from': '2022-01-01',
                'date_to': '2022-01-10',
                'orig_code': tuple(['CNSGH']),
                'dest_code': tuple(['NLRTM','BEZEE','FRLEH','DEBRV','BEANR','GBFXT','GBSOU','DEHAM'])
            }),
        ])


def test_get_avg_prices_no_data():
    mock_select = MagicMock()
    mock_select.side_effect = [
        [[['NOTINDB']]],
        [[['not_in_db']]],
        []
    ]
    with patch('rates.service.select', mock_select):
        params = {
            'origin': 'NOTINDB',
            'destination': 'not_in_db',
            'date_from': '2022-01-01',
            'date_to': '2022-01-10'
        }
        result = get_avg_prices(params)
        assert result == []
        mock_select.assert_has_calls([
            call(PORTS_QUERY, {'slug': 'NOTINDB'}),
            call(PORTS_QUERY, {'slug': 'not_in_db'}),
        ])
        # Doing it a bit differently here...
        mock_select.assert_called_with(
            AVG_PRICE_QUERY,
            {
                'date_from': '2022-01-01',
                'date_to': '2022-01-10',
                'orig_code': tuple(['NOTINDB']),
                'dest_code': tuple(['not_in_db'])
            }
        )
