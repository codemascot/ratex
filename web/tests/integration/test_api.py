import pytest

from werkzeug.test import TestResponse


@pytest.fixture(scope="function")
def endpoint():
    return '/rates/'


@pytest.fixture(scope="function")
def mock_valid_params():
    # Set up mock config values
    return {
        'origin': 'CNSGH',
        'destination': 'north_europe_main',
        'date_from': '2016-01-01',
        'date_to': '2016-01-10'
    }


@pytest.fixture(scope="function")
def mock_params_400():
    # Set up mock config values
    return  {
        'origin': 'AMS',
        'destination': 'LHR',
        'date_from': '2022-01-01'
    }


@pytest.fixture(scope="function")
def mock_params_404():
    # Set up mock config values
    return  {
        'origin': 'XXX',
        'destination': 'YYY',
        'date_from': '2022-01-01',
        'date_to': '2022-01-05'
    }


def test_get_rates_returns_200_when_valid_params(client, mock_valid_params, endpoint):
    response: TestResponse = client.get(endpoint, query_string=mock_valid_params)
    assert response.status_code == 200


def test_get_rates_returns_expected_data_when_valid_params(client, mock_valid_params, endpoint):
    response: TestResponse = client.get(endpoint, query_string=mock_valid_params)
    assert response.status_code == 200
    assert isinstance(response.json, list)
    assert all(isinstance(el, dict) for el in response.json)
    assert all('day' in el and 'average_price' in el for el in response.json)


def test_get_rates_returns_400_when_missing_params(client, endpoint, mock_params_400):
    response: TestResponse = client.get(endpoint, query_string=mock_params_400)
    assert response.status_code == 400


def test_get_rates_returns_404_when_no_data(client, endpoint, mock_params_404):
    response: TestResponse = client.get(endpoint, query_string=mock_params_404)
    assert response.status_code == 404
